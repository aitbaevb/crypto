package ab.android.crypto.api

import ab.android.crypto.model.CoinsResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("coins")
    fun getCrypto(
        @Query("limit") limit: Int,
        @Query("currency") currency: String
    ): Observable<CoinsResponse>
}