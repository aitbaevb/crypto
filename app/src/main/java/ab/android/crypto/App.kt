package ab.android.crypto

import ab.android.crypto.di.ApplicationComponent
import ab.android.crypto.di.DaggerApplicationComponent
import ab.android.crypto.di.module.ApplicationModule
import android.app.Application
import timber.log.Timber

class App: Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        instance = this
        setup()
    }

    private fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    companion object {
        lateinit var instance: App private set
    }
}