package ab.android.crypto.repo

import ab.android.crypto.BuildConfig
import ab.android.crypto.api.ApiService
import ab.android.crypto.model.Cryptocurrency
import ab.android.crypto.room.CryptoDAO
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CryptoRepo
    @Inject constructor(
        private val api: ApiService,
        private val cryptoDAO: CryptoDAO) {

    // getting crypto lisr from API
    fun getCryptoFromApi(): Observable<List<Cryptocurrency>> {
        return api.getCrypto(BuildConfig.API_LIMIT, BuildConfig.API_CURRENCY).map {
            it.coins
        }.doOnNext {
            for (coin in it)
                cryptoDAO.insertTask(coin)
        }
    }

    // get crypto list from room
    fun getFromDB(): Observable<List<Cryptocurrency>> {
        return cryptoDAO.fetchCryptos().observeOn(Schedulers.io())
    }

    // get search result list
    fun getSearchFromDB(query: String): Observable<List<Cryptocurrency>> {
        return cryptoDAO.getSearchResult(query)
    }
}