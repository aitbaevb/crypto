package ab.android.crypto.di.module

import android.content.Context
import dagger.Module
import dagger.Provides


@Module(includes = [ViewModelsModule::class])
class ApplicationModule(private val context: Context) {

    @Provides
    fun provideContext(): Context = context
}