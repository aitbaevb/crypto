package ab.android.crypto.di.module

import ab.android.crypto.api.ApiService
import ab.android.crypto.repo.CryptoRepo
import ab.android.crypto.room.CryptoDAO
import androidx.room.Dao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepoModule {

    @Provides
    @Singleton
    fun provideCryptoRepo(api: ApiService,
                          cryptoDao: CryptoDAO): CryptoRepo = CryptoRepo(api, cryptoDao)
}

