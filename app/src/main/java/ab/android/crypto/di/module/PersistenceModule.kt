package ab.android.crypto.di.module

import ab.android.crypto.room.AppDatabase
import ab.android.crypto.room.CryptoDAO
import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PersistenceModule {

  @Singleton
  @Provides
  fun provideDatabase(context: Context): AppDatabase {
    return Room
      .databaseBuilder(context.applicationContext, AppDatabase::class.java, "crypto.db")
      .build()
  }

  @Singleton
  @Provides
  fun provideCryptoDAO(database: AppDatabase): CryptoDAO {
    return database.cryptoDAO()
  }

}
