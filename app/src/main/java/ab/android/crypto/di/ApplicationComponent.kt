package ab.android.crypto.di

import ab.android.crypto.App
import ab.android.crypto.ui.MainActivity
import ab.android.crypto.di.module.ViewModelsModule
import dagger.Component
import ab.android.crypto.di.module.ApplicationModule
import ab.android.crypto.di.module.NetworkModule
import ab.android.crypto.di.module.PersistenceModule
import javax.inject.Singleton

@Component(modules = [
    PersistenceModule::class,
    ApplicationModule::class,
    NetworkModule::class,
    ViewModelsModule::class])
@Singleton
interface ApplicationComponent {

    // application
    fun inject(application: App)

    // activities
    fun inject(mainActivity: MainActivity)
}