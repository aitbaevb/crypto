package ab.android.crypto.utils

import ab.android.crypto.R
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged
import com.squareup.picasso.Picasso


class BindingAdapter {
    companion object {

        @BindingAdapter(value = ["android:icon"])
        @JvmStatic
        fun setIcon(imageView: ImageView, imageUrl: String?) {
            imageUrl?.let {
                Picasso.get().load(it).placeholder(R.drawable.placeholder).into(imageView)
            }
        }

        @BindingAdapter("android:onTextChanged")
        fun setListener(view: TextView?, onTextChanged: OnTextChanged?) {
            setListener(view, onTextChanged)
        }
    }
}