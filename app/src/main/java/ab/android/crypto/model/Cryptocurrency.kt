package ab.android.crypto.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class CoinsResponse(
    val coins: List<Cryptocurrency>
)

@Entity
data class Cryptocurrency(
    @PrimaryKey
    val id: String,
    val name: String,
    val icon: String,
    val price: Double,
    val rank: Int
)
