package ab.android.crypto.room

import ab.android.crypto.model.Cryptocurrency
import androidx.room.*
import io.reactivex.Observable

@Dao
interface CryptoDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTask(crypto: Cryptocurrency)

    @Query("SELECT * FROM Cryptocurrency")
    fun fetchCryptos(): Observable<List<Cryptocurrency>>

    @Query("SELECT * FROM Cryptocurrency WHERE UPPER(Cryptocurrency.name) LIKE UPPER(:query)")
    fun getSearchResult(query: String): Observable<List<Cryptocurrency>>

    @Update
    fun updateCrypto(crypto: Cryptocurrency)

    @Delete
    fun deleteCrypto(crypto: Cryptocurrency)

}