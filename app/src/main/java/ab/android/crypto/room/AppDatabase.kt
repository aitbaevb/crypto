package ab.android.crypto.room

import ab.android.crypto.model.Cryptocurrency
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Cryptocurrency::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cryptoDAO(): CryptoDAO
}