package ab.android.crypto.ui

import ab.android.crypto.App
import ab.android.crypto.R
import ab.android.crypto.databinding.ActivityMainBinding
import ab.android.crypto.utils.toast
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as App).component.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel

        setupErrors()
        initSwipeRefresh()
        initAdapter()
    }

    // listening for api errors
    private fun setupErrors() {
        viewModel.internetException.observe(this, {
            if (it) toast(getString(R.string.no_internet_text))
        })
        viewModel.genericException.observe(this, {
            if (it) toast(getString(R.string.smth_went_wrong_text))
        })
    }

    // initializing crypto adapter
    private fun initAdapter() {
        val adapter = CryptoAdapter()
        binding.mainRecyclerview.adapter = adapter
        viewModel.getCryptoList()
        viewModel.getCryptoFromApi()
        viewModel.cryptoLiveData.observe(this, {
            it?.let {
                adapter.submitList(it) {
                    binding.mainRecyclerview.scrollToPosition(0)
                }
            }
        })
    }

    // initializing swipe refresh layout
    private fun initSwipeRefresh() {
        viewModel.inProgress.observe(this, {
            binding.mainSwipe.isRefreshing = it
        })
        binding.mainSwipe.setOnRefreshListener {
            viewModel.getCryptoFromApi()
            binding.mainSwipe.isRefreshing = false
        }
    }
}