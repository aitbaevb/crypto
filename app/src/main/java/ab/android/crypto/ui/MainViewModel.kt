package ab.android.crypto.ui

import ab.android.crypto.BuildConfig
import ab.android.crypto.model.Cryptocurrency
import ab.android.crypto.repo.CryptoRepo
import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class MainViewModel
    @Inject constructor(private val cryptoRepo: CryptoRepo): ViewModel() {

    val cryptoLiveData = MutableLiveData<List<Cryptocurrency>>()
    val internetException = MutableLiveData<Boolean>()
    val genericException = MutableLiveData<Boolean>()
    private lateinit var timer: CountDownTimer
    val inProgress = MutableLiveData<Boolean>()
    private var query = ""
    private val compositeDisposable = CompositeDisposable()
    init {
        startTimer()
    }

    // get crypto list from room
    fun getCryptoList() {
        inProgress.postValue(true)
        compositeDisposable.add(cryptoRepo.getFromDB()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    inProgress.postValue(false)
                    cryptoLiveData.postValue(it)
                }, {
                    inProgress.postValue(false)
                })
        )
    }

    // get crypto list from api
    fun getCryptoFromApi() {
        inProgress.postValue(true)
        compositeDisposable.add(cryptoRepo.getCryptoFromApi()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                inProgress.postValue(false)
                cryptoLiveData.postValue(it)
            }, {
                when(it) {
                    is IOException -> internetException.postValue(true)
                    is HttpException -> genericException.postValue(true)
                    else -> genericException.postValue(true)
                }
                inProgress.postValue(false)
            })
        )
    }

    fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        timer.start()
        query = text.toString()
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    // timer for search event
    private fun startTimer() {
        if (::timer.isInitialized)
            timer.cancel()
        timer = object : CountDownTimer(BuildConfig.SEARCH_AFTER_MILLISECONDS, BuildConfig.SEARCH_STEP_MILLISECONDS) {
            override fun onFinish() {
                getSearchCrypto()
            }
            override fun onTick(p0: Long) {}
        }

    }

    // search crypto by name
    fun getSearchCrypto() {
        val search = "%${query.trim()}%"
        inProgress.postValue(true)
        compositeDisposable.add(
                cryptoRepo.getSearchFromDB(search)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            cryptoLiveData.postValue(it)
                            inProgress.postValue(false)
                        }, {
                            inProgress.postValue(false)
                        })
        )
    }
}