package ab.android.crypto.ui

import ab.android.crypto.R
import ab.android.crypto.databinding.CryptoItemBinding
import ab.android.crypto.model.Cryptocurrency
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class CryptoAdapter: ListAdapter<Cryptocurrency, CryptoAdapter.ViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val holder = DataBindingUtil.inflate<CryptoItemBinding>(
            LayoutInflater.from(parent.context), R.layout.crypto_item, parent, false
        )
        return ViewHolder(holder)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(getItem(position))
    }

    class ViewHolder(private val binding: CryptoItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItems(item: Cryptocurrency) {
            binding.crypto = item
            binding.executePendingBindings()
        }
    }

    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<Cryptocurrency>() {
            override fun areItemsTheSame(oldItem: Cryptocurrency, newItem: Cryptocurrency): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Cryptocurrency, newItem: Cryptocurrency): Boolean {
                return oldItem == newItem
            }
        }
    }
}